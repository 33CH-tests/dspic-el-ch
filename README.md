## dsPIC-EL-CH

dsPIC-EL board for dsPIC33CH128MP506 and dsPIC33CH512MP506

Tags represent orders:

* Y6-2069446A - JLCPCB 2019-02-08
* Y5-2069446A - JLCPCB 2019-01-19
* Y4-2069446A - JLCPCB 2018-12-24
* Y3-2069446A - JLCPCB 2018-12-23 cancelled
* Y2-2069446A - JLCPCB 2018-11-27

---

![Board photo-realistic](dsPIC-EL-CH.png)

---

![Schematic](dsPIC-EL-CH-SCHEMATIC.png)

---

